require "mongo_db_schema/version"
require 'rails'
require "mongo_db_schema/app/models/category.rb"
require "mongo_db_schema/app/models/language.rb"
require "mongo_db_schema/app/models/audio.rb"
require "mongo_db_schema/app/models/audio_category.rb"
require "mongo_db_schema/app/models/user_report_log.rb"
require "mongo_db_schema/app/models/country.rb"
require "mongo_db_schema/app/models/message_template.rb"
require "mongo_db_schema/app/models/currency.rb"
require "mongo_db_schema/app/models/preference.rb"
require "mongo_db_schema/app/models/reward.rb"
require "mongo_db_schema/app/models/subscription.rb"
require "mongo_db_schema/app/models/subscription_feature.rb"
require "mongo_db_schema/app/models/setting.rb"
require "mongo_db_schema/app/models/face_filter.rb"

module MongoDbSchema
  class Engine < Rails::Engine
  end
  class Error < StandardError; end
end
