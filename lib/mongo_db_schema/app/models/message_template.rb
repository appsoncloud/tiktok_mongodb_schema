module MongoDbSchema
  class MessageTemplate
    include Mongoid::Document
    include Mongoid::Timestamps

    field :message, type: String
    field :note, type: String
    field :active, type: Boolean, default: true

    validates :message, presence: true

    scope :active, -> { where(active: true) }
  end
end