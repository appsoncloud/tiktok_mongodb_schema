module MongoDbSchema
  class Subscription
    include Mongoid::Document
    include Mongoid::Timestamps

    field :plan_type, type: String
    field :price, type: Integer
    field :plan_period, type: String
    field :country, type: String
    field :super_like_count, type: Integer, default: 0
    field :active, type: Boolean, default: true

    validates :plan_type, :price, :plan_period, :country, presence: true

    scope :active, -> { where(active: true) }
  end
end
