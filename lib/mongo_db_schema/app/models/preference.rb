module MongoDbSchema
  class Preference
    include Mongoid::Document
    include Mongoid::Slug
    include Mongoid::Timestamps
    require 'will_paginate/mongoid'

    field :name, type: String
    field :description, type: String
    field :active, type: Boolean, default: true
    slug :name

    validates :name, presence: true

    scope :active, -> { where(active: true) }
  end
end
