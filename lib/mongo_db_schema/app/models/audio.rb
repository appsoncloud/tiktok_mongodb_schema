
module MongoDbSchema
  class Audio
    include Mongoid::Document
    include Mongoid::Timestamps
    require 'will_paginate/mongoid'
    require 'will_paginate/array'

    field :title, type: String
    field :description, type: String
    field :audio_category_id, type: String
    field :active, type: Boolean, default: true

    validates :title, :audio, presence: true
  end
end