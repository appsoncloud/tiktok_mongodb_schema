
module MongoDbSchema
  class Language
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Slug

    field :name, type: String
    field :country, type: String
    field :active, type: Boolean, default: true
    slug :name
    
    validates :name, presence: true
  end
end