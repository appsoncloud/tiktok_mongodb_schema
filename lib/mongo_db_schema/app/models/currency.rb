module MongoDbSchema
  class Currency
    include Mongoid::Document
    include Mongoid::Timestamps

    field :name, type: String
    field :abbreviation, type: String
    field :symbol, type: String
    field :country_id, type: String

    validates_uniqueness_of :name

    belongs_to :country
  end
end