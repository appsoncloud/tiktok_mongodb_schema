module MongoDbSchema
  class SubscriptionFeature
    include Mongoid::Document
    include Mongoid::Timestamps

    field :title, type: String
    field :description, type: String
    field :subscription_id, type: String

    validates :title, :description, presence: true
  end
end
