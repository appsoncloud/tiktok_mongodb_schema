module MongoDbSchema
  class Category
    include Mongoid::Document
    include Mongoid::Slug
    include Mongoid::Timestamps
  
    field :name, type: String
    field :note, type: String
    field :active, type: Boolean, default: true
    slug :name
  
    validates :name, :image, presence: true

    scope :active, -> { where(active: true) }
  end  
end