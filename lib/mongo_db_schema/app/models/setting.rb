module MongoDbSchema
  class Setting
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Slug
  
    field :name, type: String
    field :value, type: String
    slug :name
  
    validates :value, presence: true
    validates :name, format: { with: /\A[a-zA-Z_]+\Z/ }, presence: true

    class << self
      def method_missing(name)
        where(name: name).first.try(:value) || ''
      end
    end
  end
end