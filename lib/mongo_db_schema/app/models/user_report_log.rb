module MongoDbSchema
  class UserReportLog
    include Mongoid::Document
    include Mongoid::Timestamps

    field :reporter_user_id, type: String
    field :reporter_name, type: String
    field :reporter_email, type: String
    field :user_id, type: String
    field :reason, type: String
   end
end