module MongoDbSchema
  class Reward
    include Mongoid::Document
    include Mongoid::Timestamps
    include SimpleEnum::Mongoid

    field :title, type: String
    field :sub_title, type: String
    field :content, type: String
    field :note, type: String
    field :message, type: String
    field :category, type: Integer
    field :coins, type: Integer
    field :active, type: Boolean, default: true

    validates :message, :category, :coins, presence: true
    as_enum :category, %i{referral like}, pluralize_scopes: false

    scope :active, -> { where(active: true) }
  end
end
