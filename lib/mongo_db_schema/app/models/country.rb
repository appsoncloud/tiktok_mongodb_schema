module MongoDbSchema
  class Country
    include Mongoid::Document
    include Mongoid::Slug
    include Mongoid::Timestamps
  
    field :name, type: String
    field :code, type: String
    field :dial_code, type: String
    field :active, type: Boolean, default: true
    slug :name
  
    validates :name, :code, presence: true

    scope :active, -> { where(active: true) }

    has_one :currency
  end  
end