module MongoDbSchema
  class FaceFilter
    include Mongoid::Document
    include Mongoid::Timestamps

    field :type, type: String
    field :active, type: Boolean, default: true

    scope :active, -> { where(active: true) }
  end
end