require 'rails/generators'

module MongoDbSchema
  class InitializerGenerator < Rails::Generators::Base
    source_root File.expand_path('templates', __dir__)
    def create_initializer_file
      copy_file 'test.rb', 'config/initializers/test.rb'
    end
  end
end